# Yammer Archiver

## Why would I want to use it?

To create an offline HTML archive of your Yammer content.

## How I use it?

1. Download the yammer-archiver.html file and save locally. 
2. Edit the script section shown below. set the values for **network**, **feedType** and **feedId**
as required.


    // EDIT network, feedType, feedId as required
    yam.connect.embedFeed({
    	 container: '#embedded-feed'
    	,network: 'thomsonreuters.com' // network permalink
    	,feedType:'user' // can be 'group', 'topic', or 'user'
    	,feedId:'822607' // feed ID
    });
 
See the [Yammer Embed Install guide](https://www.yammer.com/company/embed) for information on these
settings.

3. Save the edited  yammer-archiver.html file
4. Open in a browser.
5. Click the "Sign in with Yammer button". If you have not used the Yammer Embed widget you will
be prompted to Allow access to the widget. 
6. The script will starting downloading items. 
7. When complete (it may take a couple of minutes) it will prompt you to save the result.
8. Use the standard menu item **File**->**Save page as** with the Web page complete format.

A html file and folder with the chosen name will be created. The folder contains the images and css
for the archive. 
   
## How does it work?

The html uses Yammer Embed plus a small jQuery Javascript script. 
The script does the following:
 
1. "clicks"  the **More** button until no more Yams are loaded. 
2. "clicks" any **Show older replies** until no more are found.
3. Cleanup the page by removing unneeded items, including itself. 
4. Prompts you to save the page.

## Performance
For me, about  6500 "yams" with around 300 show olders took around 20mins. A 5megs html file was
created with an additional 5meg associated folder. 

Sometimes clicking on the "Show older replies" generates a server permission error. This maybe a 
Yammer bug or possibly some throttling. The code attempts to detect this and proceed to the
remaining items.

## Tested with

Firefox 12, Chrome 18 in May 2012. Changes to Yammer may invalidate this approach at some future point.
I prefer the Firefox save page as it saves resources, such as images, with a meaningful extension.


[![Flattr Button](http://api.flattr.com/button/button-static-50x60.png "Flattr This!")](https://flattr.com/thing/685470/Yammer-Archiver "yammer-archive")
All code is available under the BSD license.